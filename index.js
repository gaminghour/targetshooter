let target; 
let shooter;
let bullets=[];
let delay=10;
function setup() {
	createCanvas(windowWidth,windowHeight);
	target = new Target();
	shooter= new Shooter();
}

function draw(){
	background('white');
	target.update();
	target.show();
	shooter.move(true);
	shooter.show();

	if(mouseIsPressed){
		// shooter.move(mouseX>windowWidth/2);  //shooter movement restricted
		if(delay===0){
			bullets.push(new Bullet(shooter.x,shooter.y));
			delay=3;
		}
		delay--;

	}

	for (var i = bullets.length - 1; i >= 0; i--) {
		bullets[i].move();
		bullets[i].show();
	}
}

function windowResized(){
	resizeCanvas(windowWidth,windowHeight);
}


class Target{
	constructor(){
		this.x = 0;
		this.y = 10;
		this.width = 130;
		this.height = 15;
		this.color = 0;
		this.dir = 10;
	}

	update(){
		this.x = this.x +this.dir ; 
		if(this.x+this.width>= windowWidth){
			this.dir = -10; 
		}
		if(this.x <= 0){
			this.dir =10;
		}
	}

	show(){
		fill(0);
		rect(this.x,this.y,this.width,this.height);
	}
}

class Shooter{
	constructor(){
		this.width = 60; 
		this.height = 10;
		this.x = (windowWidth/2) - (this.width/2);
		this.y = windowHeight - 150;
	}
	move(right){
		// if(right){
		// 	if(this.x<windowWidth){
		// 		this.x+=5;
		// 	}
		// }
		// else{
		// 	if(this.x>0){
		// 		this.x-=5;
		// 	}
		// }

		this.x = mouseX;

	}
	show(){

		fill(0);
		rect(this.x,this.y,this.width,this.height);
		rect(this.x+20,this.y-40,20,40)
	}
}

class Bullet{
	constructor(x,y){
		this.width=10;
		this.height=10;
		this.x=x+30;
		this.y=y;
	}

	move(){
		this.y-=10;
	}
	show(){
		fill(0);
		ellipse(this.x,this.y,this.width,this.height);
	}
}